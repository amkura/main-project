import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { map, switchMap, withLatestFrom } from "rxjs/operators";
import { Recipe } from "../recipe.model";
import * as RecipeActions from './recipe.actions';
import * as fromApp from '../../store/app.reducer';
@Injectable()
export class RecipeEffects {
    constructor(
        private actions$: Actions,
        private http: HttpClient,
        private store: Store<fromApp.AppState>
    ) {}

    @Effect()
    fetchRecipes$ = this.actions$
        .pipe(
            ofType(RecipeActions.FETCH_RECIPES),
            switchMap(() => {
                return this.http
                .get<Recipe[]>('https://ng-course-recipe-book-e454d-default-rtdb.europe-west1.firebasedatabase.app/recipes.json')
            }),
            map((recipes) => {
                return recipes.map(recipe => ({
                  ...recipe,
                  ingredients: recipe.ingredients ? recipe.ingredients: []
                }))
            }),
            map(recipes => new RecipeActions.SetRecipes(recipes))
        )

    @Effect({dispatch: false})
    storeRecipes$ = this.actions$
        .pipe(
            ofType(RecipeActions.STORE_RECIPES),
            withLatestFrom(this.store.select('recipes')),
            switchMap(([actionData, recipesState]) => {
                return this.http
                    .put(
                        'https://ng-course-recipe-book-e454d-default-rtdb.europe-west1.firebasedatabase.app/recipes.json',
                        recipesState.recipes
                    )
            })
        )
}