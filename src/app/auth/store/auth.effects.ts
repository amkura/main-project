import {Actions, createEffect, Effect, ofType} from "@ngrx/effects";
import * as AuthActions from './auth.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {AuthService} from "../auth.service";
import {HttpClient} from "@angular/common/http";
import { environment} from "../../../environments/environment";
import {of, throwError} from "rxjs";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../user.model";

export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

const handleAuthentication = (expiresIn: number, email: string, userId: string, token: string) => {
  console.log('Expires in : ', expiresIn);
  const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
  const user = new User(email, userId, token, expirationDate);
  console.log('Authenticated user: ', user);
  localStorage.setItem('userData', JSON.stringify(user));
  return new AuthActions.AuthenticateSuccess({
    email: email,
    userId: userId,
    token: token,
    expirationDate: expirationDate,
    redirect: true
  });
}

const handleError = (errorRes: any) => {
  let errorMessage = 'An unknown error occured!';

  if(!errorRes.error || !errorRes.error.error) {
    return of(new AuthActions.AuthenticateFail(errorMessage));
  }

  switch(errorRes.error.error.message) {
    case 'EMAIL_EXISTS':
      errorMessage = 'This email exists already';
      break;
    case 'EMAIL_NOT_FOUND':
      errorMessage = 'This email does not exist';
      break;
    case 'INVALID_PASSWORD':
      errorMessage = 'This password is not valid';
      break;
  }
  return of(new AuthActions.AuthenticateFail(errorMessage));
}

// needs it so we can inject things into this class
@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  @Effect()
  authSignup$ = this.actions$.pipe(
    ofType(AuthActions.SIGNUP_START),
    switchMap((signupAction: AuthActions.SignupStart) => {
      return this.http
        .post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.firebaseAPIKey}`, {
          email: signupAction.payload.email,
          password: signupAction.payload.password,
          returnSecureToken: true
        })
        .pipe(
          tap((resData) => {
            this.authService.setLogoutTimer(+resData.expiresIn * 1000);
          }),
          map(resData => handleAuthentication(+resData.expiresIn, resData.email, resData.localId, resData.idToken)),
          catchError(errorRes => handleError(errorRes)),
        )
    })
  );

  @Effect()
  authLogin$ = this.actions$.pipe(
      ofType(AuthActions.LOGIN_START),
      switchMap((authData: AuthActions.LoginStart) => {
        return this.http
          .post<AuthResponseData>(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.firebaseAPIKey}`, {
            email: authData.payload.email,
            password: authData.payload.password,
            returnSecureToken: true
          })
          .pipe(
            tap((resData) => {
              this.authService.setLogoutTimer(+resData.expiresIn * 1000);
            }),
            map(resData => handleAuthentication(+resData.expiresIn, resData.email, resData.localId, resData.idToken)),
            catchError(errorRes => handleError(errorRes)),
          )
      }),
    );

  // we have to let ngrx know that we will not be dispatching any action to avoid errors
  @Effect({dispatch: false})
  authRedirect = this.actions$.pipe(
    ofType(AuthActions.AUTHENTICATE_SUCCESS),
    tap((authSuccessAction: AuthActions.AuthenticateSuccess) => {
      if(authSuccessAction.payload.redirect) {
        this.router.navigate(['/']);
      }
    })
  )

  @Effect()
  autoLogin$ = this.actions$.pipe(
    ofType(AuthActions.AUTO_LOGIN),
    map(() => {
      const userData: {
        email: string;
        id: string;
        _token: string;
        _tokenExpirationDate: string;
      } = JSON.parse(localStorage.getItem('userData')!);
      if(!userData) {
        return {type: 'DUMMY'}
      }

      const {email, id, _token, _tokenExpirationDate} = userData;
      const loadedUser = new User(email, id,  _token, new Date(_tokenExpirationDate));

      if(loadedUser.token) {
        const expirationDuration = new Date(_tokenExpirationDate).getTime() - new Date().getTime();
        this.authService.setLogoutTimer(expirationDuration);
        return new AuthActions.AuthenticateSuccess({
          email: email,
          userId: id,
          token: _token,
          expirationDate: new Date(_tokenExpirationDate),
          redirect: false
        });
        // this.autoLogout(expirationDuration);
      }
      return {
        type: 'DUMMY'
      };
    })
  )

  @Effect({dispatch: false})
  authLogout$ = this.actions$.pipe(
    ofType(AuthActions.LOGOUT),
    tap(() => {
      this.router.navigate(['/auth']);
      this.authService.clearLogoutTimer();
      localStorage.removeItem('userData');
    })
  )


}
