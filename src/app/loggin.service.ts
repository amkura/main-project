import {Injectable} from "@angular/core";

// @Injectable({providedIn: 'root'})
export class LoggingService {
  lastlog: string;

  printLog(message: string) {
    console.log('new message: ', message);
    console.log('old message: ', this.lastlog);
    this.lastlog = message;
  }
}
