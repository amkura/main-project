import {Ingredient} from "../../shared/ingredient.model";
import * as ShoppingListActions from "./shopping-list.action";
import {Action} from "@ngrx/store";

export interface State {
  ingredients: Ingredient[],
  editedIngredient: Ingredient | null,
  editedIngredientIndex: number
}

const initialState: State = {
  ingredients: [
    new Ingredient('Apples', 4),
    new Ingredient('Tomatoes', 10),
  ],
  editedIngredient: null,
  editedIngredientIndex: -1
};

export function shoppingListReducer(state : State = initialState, incomingAction : Action) {
  const action = incomingAction as ShoppingListActions.ShoppingListActions;
  switch(action.type) {
    case ShoppingListActions.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: [
          ...state.ingredients,
          action.payload
        ]
      }
    case ShoppingListActions.ADD_INGREDIENTS:
      return {
        ...state,
        ingredients: [
          ...state.ingredients,
          ...action.payload
        ]
      }
    case ShoppingListActions.START_EDIT:
      return {
        ...state,
        editedIngredientIndex: action.payload,
        editedIngredient: {...state.ingredients[action.payload]}
      };
    case ShoppingListActions.STOP_EDIT: {
      return {
        ...state,
        editedIngredientIndex: -1,
        editedIngredient: null
      };
    }
    case ShoppingListActions.UPDATE_INGREDIENT:
      console.log('Ingredients: ', state.ingredients);
      const ingredientToEdit = state.ingredients[state.editedIngredientIndex];
      const updatedIngredient = {
        ...ingredientToEdit,
        ...action.payload
      };

      console.log('Updated ingredient: ', updatedIngredient);

      const updatedIngredients = [...state.ingredients];
      updatedIngredients[state.editedIngredientIndex] = updatedIngredient;

      console.log('Updated ingredients: ', updatedIngredients);

      return {
        ...state,
        ingredients: updatedIngredients,
        editedIngredientIndex: -1,
        editedIngredient: null
      };
    case ShoppingListActions.DELETE_INGREDIENT:
      return {
        ...state,
        ingredients: state.ingredients.filter((ingredient, index) => index !== state.editedIngredientIndex),
        editedIngredientIndex: -1,
        editedIngredient: null
      }
    default:
      return state;
  }
}
